// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "hbsbombermanGameModeBase.generated.h"

class APowerUp;
class APlayerCharacter;

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EGameResult : uint8
{
	GR_Win 		UMETA(DisplayName = "Win"),
	GR_Draw 	UMETA(DisplayName = "Draw")
};

UCLASS()
class HBSBOMBERMAN_API AhbsbombermanGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	AhbsbombermanGameModeBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void PlayerDead(int PlayerNumber, APlayerCharacter* PlayerCharacter);

	UFUNCTION(BlueprintCallable)
		void SpawnRandomPickUP(FVector Location);

	UFUNCTION(BlueprintImplementableEvent)
		void EndRound(EGameResult GameResult, int PlayerWinner);

	UPROPERTY(EditDefaultsOnly)
		float SpawnChance;

	UPROPERTY(EditDefaultsOnly)
		TArray<TSubclassOf<APowerUp>> SpawnOptions;

	UPROPERTY(EditDefaultsOnly)
		float GameTime;

	UPROPERTY(EditDefaultsOnly)
		float DelayStartGame;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float GameTimeLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float DelayStartGameLeft;

private:

	TMap<int, APlayerCharacter*> SpawnedCharacters;
};
