// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlainBomb.generated.h"

class USphereComponent;

UCLASS()
class APlainBomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlainBomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Explode();
	void ReceiveExplosion();
	void ActivateBomb(AActor* BombOwnerActor, float TimeExplode, int BombTileRange);
	
	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* OverlappedComponent,	AActor* OtherActor,	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USphereComponent* SphereComponent;

private:

	void LineTraceObjects(FVector Direction, int Tiles);

	bool ExplosionTriggered;
	AActor* BombOwner;
	FTimerHandle BombExplodeTimerHandle;
	TArray<AActor*> OverlappingActors;
	int TileRange;
};
