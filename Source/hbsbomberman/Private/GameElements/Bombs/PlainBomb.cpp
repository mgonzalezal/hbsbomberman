// Fill out your copyright notice in the Description page of Project Settings.


#include "PlainBomb.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "../PowerUps/PowerUp.h"
#include "../Walls/Wall.h"
#include "../../Characters/PlayerCharacter.h"

// Sets default values
APlainBomb::APlainBomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &APlainBomb::BeginOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &APlainBomb::EndOverlap);
	RootComponent = SphereComponent;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void APlainBomb::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlainBomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlainBomb::Explode()
{
	if (!ExplosionTriggered) {
		ExplosionTriggered = true;
		if (APlayerCharacter* BombOwnerTmp = Cast<APlayerCharacter>(BombOwner))
		{
			BombOwnerTmp->BombExploded(this);
		}

		//UP
		LineTraceObjects(FVector(1.0f, 0.0f, 0.0f), TileRange);

		//DOWN
		LineTraceObjects(FVector(-1.0f, 0.0f, 0.0f), TileRange);

		//LEFT
		LineTraceObjects(FVector(0.0f, -1.0f, 0.0f), TileRange);

		//RIGHT
		LineTraceObjects(FVector(0.0f, 1.0f, 0.0f), TileRange);

		Destroy();
	}
}

void APlainBomb::ReceiveExplosion()
{
	GetWorld()->GetTimerManager().ClearTimer(BombExplodeTimerHandle);
	Explode();
	//GetWorld()->GetTimerManager().SetTimer(BombExplodeTimerHandle, this, &APlainBomb::Explode, 0.1f, false);
}

void APlainBomb::ActivateBomb(AActor* BombOwnerActor, float TimeExplode, int BombTileRange)
{
	GetWorld()->GetTimerManager().SetTimer(BombExplodeTimerHandle, this, &APlainBomb::Explode, TimeExplode, false);
	BombOwner = BombOwnerActor;
	TileRange = BombTileRange;
	ExplosionTriggered = false;
}

void APlainBomb::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		if (OverlappingActors.Num() == 0) {
			SphereComponent->SetCollisionObjectType(ECC_GameTraceChannel2);
			SphereComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel3, ECollisionResponse::ECR_Overlap);
		}
		OverlappingActors.Add(PlayerCharacter);
	}
}

void APlainBomb::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		OverlappingActors.Remove(PlayerCharacter);
		if (OverlappingActors.Num() == 0) 
		{
			SphereComponent->SetCollisionObjectType(ECC_GameTraceChannel1);
			SphereComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel3, ECollisionResponse::ECR_Block);
		}
	}
}

void APlainBomb::LineTraceObjects(FVector Direction, int Tiles)
{
	//Check for a wall first to determine the lenght of the explosion
	FHitResult outHit;
	float ExplosionDistance = 100.0f * Tiles + 50.0f;
	float DistanceCheckForPlayers = ExplosionDistance;
	if (GetWorld()->LineTraceSingleByObjectType(outHit, GetActorLocation(), GetActorLocation() + Direction * ExplosionDistance, ECC_GameTraceChannel5))
	{
		DistanceCheckForPlayers = outHit.Distance + 50.0f;
	}


	//Multi line trace in C++ does not get multiple hits even if they are set as overlap, doing multiple linetraces.
	FCollisionQueryParams CollisionParams;
	for (size_t i = 0; i < 3; i++)
	{
		float DistanceRay = 30.0f;
		FVector DirectionMultiRay = FVector::ZeroVector;
		if (Direction.X > 0.0f || Direction.X < 0.0f)
		{
			DirectionMultiRay = FVector(0.0f, -DistanceRay + DistanceRay * i, 0.0f);
		}
		else
		{
			DirectionMultiRay = FVector(-DistanceRay + DistanceRay * i, 0.0f, 0.0f);
		}

		bool Hit = true;
		while (Hit)
		{
			if (GetWorld()->LineTraceSingleByChannel(outHit, GetActorLocation() + DirectionMultiRay, GetActorLocation() + DirectionMultiRay + Direction * DistanceCheckForPlayers, ECC_Visibility, CollisionParams))
			{
				DrawDebugLine(GetWorld(), GetActorLocation() + DirectionMultiRay, GetActorLocation() + DirectionMultiRay + Direction * outHit.Distance, FColor::Blue, false, 1.0f);
				CollisionParams.AddIgnoredActor(outHit.Actor.Get()); // Adding actors that we have already collided with to detect others.
				if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(outHit.Actor))
				{
					PlayerCharacter->ReceiveExplosion();
				}

				if (APlainBomb* PlainBomb = Cast<APlainBomb>(outHit.Actor))
				{
					PlainBomb->ReceiveExplosion();
				}

				if (AWall* Wall = Cast<AWall>(outHit.Actor))
				{
					Wall->ReceiveExplosion();
				}

				if (APowerUp* PowerUp = Cast<APowerUp>(outHit.Actor))
				{
					PowerUp->ReceiveExplosion();
				}
			}
			else
			{
				Hit = false;
				DrawDebugLine(GetWorld(), GetActorLocation() + DirectionMultiRay, GetActorLocation() + DirectionMultiRay + Direction * DistanceCheckForPlayers, FColor::Red, false, 1.0f);
			}
		}		
	}
}
