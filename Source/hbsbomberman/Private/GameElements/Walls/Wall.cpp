// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"

#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "hbsbomberman/hbsbombermanGameModeBase.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(RootComponent);

	IsDestroyable = true;
}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWall::ReceiveExplosion()
{
	if (IsDestroyable) 
	{
		if (AhbsbombermanGameModeBase* hbsbombermanGameModeBase = Cast<AhbsbombermanGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())))
		{
			hbsbombermanGameModeBase->SpawnRandomPickUP(GetActorLocation());
		}
		Destroy();
	}
}

