// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PowerUp.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPowerUpType : uint8
{
	PUT_IncrementRange 	UMETA(DisplayName = "IncrementRange"),
	PUT_IncrementBombs 	UMETA(DisplayName = "IncrementBombs"),
	PUT_IncrementSpeed	UMETA(DisplayName = "IncrementSpeed"),
	PUT_RemoteControlledBombs	UMETA(DisplayName = "RemoteControlledBombs")
};

class UBoxComponent;

UCLASS()
class APowerUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APowerUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	void ReceiveExplosion();
	void SetImmune(bool Immune);

	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UBoxComponent* BoxComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		EPowerUpType PowerUpType;

	//Only used in case of a temporal pick up
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float TimePowerUpEffective;

private:

	bool IsImmune; //So bombs do not destroy us as soon as we spawn.
	FTimerHandle ImmuneTimerHandle;
};
