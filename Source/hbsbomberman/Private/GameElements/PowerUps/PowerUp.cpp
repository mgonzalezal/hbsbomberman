// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUp.h"

#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "../../Characters/PlayerCharacter.h"
#include "TimerManager.h"

// Sets default values
APowerUp::APowerUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &APowerUp::BeginOverlap);
	RootComponent = BoxComponent;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(RootComponent);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void APowerUp::BeginPlay()
{
	Super::BeginPlay();

	IsImmune = false;	
}

void APowerUp::ReceiveExplosion()
{
	if (!IsImmune)
	{
		Destroy();
	}
}

void APowerUp::SetImmune(bool Immune)
{
	IsImmune = Immune;
	if (IsImmune) {
		FTimerDelegate RemoteBombsDelegate = FTimerDelegate::CreateUObject(this, &APowerUp::SetImmune, false);
		GetWorld()->GetTimerManager().SetTimer(ImmuneTimerHandle, RemoteBombsDelegate, 0.2f, false); // Allow small delay for the power up not to be destroyed as soon as it respawns.
	}
}

void APowerUp::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		switch (PowerUpType)
		{
		case EPowerUpType::PUT_IncrementRange:
			PlayerCharacter->IncreaseTileRangeBomb(1);
			break;
		case EPowerUpType::PUT_IncrementBombs:
			PlayerCharacter->IncreaseBombCount(1);
			break;
		case EPowerUpType::PUT_IncrementSpeed:
			PlayerCharacter->IncreaseMovementSpeed(0.1f);
			break;
		case EPowerUpType::PUT_RemoteControlledBombs:
			PlayerCharacter->EnableRemoteBomb(true, TimePowerUpEffective);
			break;
		default:
			break;
		}

		Destroy();
	}
}
