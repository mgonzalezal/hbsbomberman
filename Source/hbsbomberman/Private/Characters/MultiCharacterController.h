// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MultiCharacterController.generated.h"

class APlayerCharacter;

UCLASS()
class AMultiCharacterController : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMultiCharacterController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		virtual void P1VerticalInput(float value);

	UFUNCTION()
		virtual void P1HorizontalInput(float value);

	UFUNCTION()
		virtual void P2VerticalInput(float value);

	UFUNCTION()
		virtual void P2HorizontalInput(float value);

	UFUNCTION()
		virtual void P1Bomb();

	UFUNCTION()
		virtual void P2Bomb();

	UFUNCTION()
		virtual void P1RemoteBomb();

	UFUNCTION()
		virtual void P2RemoteBomb();

private:

	TMap<int, APlayerCharacter*> SpawnedCharacters;
};
