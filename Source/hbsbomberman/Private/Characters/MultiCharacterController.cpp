// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiCharacterController.h"

#include "Kismet/GameplayStatics.h"
#include "PlayerCharacter.h"

// Sets default values
AMultiCharacterController::AMultiCharacterController()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMultiCharacterController::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Characters;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerCharacter::StaticClass(), Characters);

	for (size_t i = 0; i < Characters.Num(); i++)
	{
		APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(Characters[i]);
		SpawnedCharacters.FindOrAdd(PlayerCharacter->PlayerNumber, PlayerCharacter);
	}
}

// Called every frame
void AMultiCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMultiCharacterController::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


	PlayerInputComponent->BindAxis("P1Vertical", this, &AMultiCharacterController::P1VerticalInput);
	PlayerInputComponent->BindAxis("P1Horizontal", this, &AMultiCharacterController::P1HorizontalInput);
	PlayerInputComponent->BindAxis("P2Vertical", this, &AMultiCharacterController::P2VerticalInput);
	PlayerInputComponent->BindAxis("P2Horizontal", this, &AMultiCharacterController::P2HorizontalInput);

	PlayerInputComponent->BindAction("P1Bomb", EInputEvent::IE_Pressed, this, &AMultiCharacterController::P1Bomb);
	PlayerInputComponent->BindAction("P2Bomb", EInputEvent::IE_Pressed, this, &AMultiCharacterController::P2Bomb);
	PlayerInputComponent->BindAction("P1RemoteBomb", EInputEvent::IE_Pressed, this, &AMultiCharacterController::P1RemoteBomb);
	PlayerInputComponent->BindAction("P2RemoteBomb", EInputEvent::IE_Pressed, this, &AMultiCharacterController::P2RemoteBomb);
}

void AMultiCharacterController::P1VerticalInput(float value)
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(0))
	{
		PlayerCharacter->VerticalInput(value);
	}
}

void AMultiCharacterController::P1HorizontalInput(float value)
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(0))
	{
		PlayerCharacter->HorizontalInput(value);
	}
}

void AMultiCharacterController::P2VerticalInput(float value)
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(1))
	{
		PlayerCharacter->VerticalInput(value);
	}
}

void AMultiCharacterController::P2HorizontalInput(float value)
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(1))
	{
		PlayerCharacter->HorizontalInput(value);
	}
}

void AMultiCharacterController::P1Bomb()
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(0))
	{
		PlayerCharacter->DropBomb();
	}
}

void AMultiCharacterController::P2Bomb()
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(1))
	{
		PlayerCharacter->DropBomb();
	}
}

void AMultiCharacterController::P1RemoteBomb()
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(0))
	{
		PlayerCharacter->TriggerBombs();
	}
}

void AMultiCharacterController::P2RemoteBomb()
{
	if (APlayerCharacter* PlayerCharacter = *SpawnedCharacters.Find(1))
	{
		PlayerCharacter->TriggerBombs();
	}
}

