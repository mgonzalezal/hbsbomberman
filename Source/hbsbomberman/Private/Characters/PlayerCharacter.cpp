// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "hbsbomberman/hbsbombermanGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Horizontal = FVector::ZeroVector;
	Vertical = FVector::ZeroVector;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	BombsAvailable = MaxBombs;
	MovementSpeed = BaseMovementSpeed;
	TileRange = BaseTileRange;
	RemoteBombsEnabled = false;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector Movement = Horizontal + Vertical;
	Movement.Normalize();
	AddMovementInput(Movement, MovementSpeed);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);	
}

void APlayerCharacter::VerticalInput(float value)
{
	Horizontal = FVector(1.0f, 0.0f, 0.0f) * value;
}

void APlayerCharacter::HorizontalInput(float value)
{
	Vertical = FVector(0.0f, 1.0f, 0.0f) * value;
}

void APlayerCharacter::DropBomb()
{
	if (BombsAvailable > 0 && BombSpawn && (!RemoteBombsEnabled || (RemoteBombsEnabled && SpawnedBombs.Num() == 0)))
	{
		//Need to get a proper position to drop the bomb, the original one places the bomb in the center of the tile.
		FVector position = GetNearestRoundedPositon(GetActorLocation());
		FTransform transform;
		transform.SetLocation(position);
		if (APlainBomb* PlainBomb = GetWorld()->SpawnActor<APlainBomb>(BombSpawn, transform)) 
		{
			BombsAvailable--;
			PlainBomb->ActivateBomb(this, BombTimeExplode, TileRange);
			SpawnedBombs.Add(PlainBomb);
		}
	}
}

void APlayerCharacter::BombExploded(AActor* PlainBomb)
{
	SpawnedBombs.Remove(PlainBomb);
	AddBomb(1);
}

void APlayerCharacter::AddBomb(int BombsToAdd)
{
	BombsAvailable++;
}

void APlayerCharacter::ChangeToPawnOverlap(int BombsToAdd)
{
	GetCapsuleComponent()->SetCollisionObjectType(ECC_GameTraceChannel3);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
}

void APlayerCharacter::ChangeToNormalPawn(int BombsToAdd)
{
	GetCapsuleComponent()->SetCollisionObjectType(ECC_Pawn);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Block);
}

void APlayerCharacter::TriggerBombs()
{
	if (RemoteBombsEnabled)
	{
		for (size_t i = 0; i < SpawnedBombs.Num(); i++)
		{
			Cast<APlainBomb>(SpawnedBombs[i])->Explode();
		}
	}
}

void APlayerCharacter::ReceiveExplosion()
{
	if (AhbsbombermanGameModeBase* hbsbombermanGameModeBase = Cast<AhbsbombermanGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		hbsbombermanGameModeBase->PlayerDead(PlayerNumber, this);
	}
	Destroy();
}

void APlayerCharacter::IncreaseMovementSpeed(float Increase)
{
	MovementSpeed += Increase;
}

void APlayerCharacter::IncreaseBombCount(int Increase)
{
	MaxBombs += Increase;
	AddBomb(Increase);
}

void APlayerCharacter::IncreaseTileRangeBomb(int Increase)
{
	TileRange += Increase;
}

void APlayerCharacter::EnableRemoteBomb(bool Enable, float TimeEnabled)
{
	if (Enable) 
	{
		FTimerDelegate RemoteBombsDelegate = FTimerDelegate::CreateUObject(this, &APlayerCharacter::EnableRemoteBomb, false, 0.0f);
		GetWorld()->GetTimerManager().SetTimer(RemoteBombsTimerHandle, RemoteBombsDelegate, TimeEnabled, false);
	}
	else {
		GetWorld()->GetTimerManager().ClearTimer(RemoteBombsTimerHandle);
	}

	RemoteBombsEnabled = Enable;
}

FVector APlayerCharacter::GetNearestRoundedPositon(FVector position)
{
	int XPos = position.X;
	int YPos = position.Y;

	int remainder = abs(XPos) % 100;
	if (remainder > 0)
	{
		if (XPos > 0)
		{
			XPos = XPos + 100 - remainder;
		}
		else
		{
			XPos = -(abs(XPos) - remainder);
		}
	}

	remainder = abs(YPos) % 100;
	if (remainder > 0)
	{
		if (YPos > 0)
		{
			YPos = YPos + 100 - remainder;
		}
		else
		{
			YPos = -(abs(YPos) - remainder);
		}
	}

	return FVector(XPos - 50.0f, YPos - 50.0f, position.Z);
}
