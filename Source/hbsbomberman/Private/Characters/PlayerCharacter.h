// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../GameElements/Bombs/PlainBomb.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		virtual void VerticalInput(float value);

	UFUNCTION(BlueprintCallable)
		virtual void HorizontalInput(float value);

	UFUNCTION(BlueprintCallable)
		virtual void DropBomb();

	UFUNCTION(BlueprintCallable)
		virtual void BombExploded(AActor* PlainBomb);

	UFUNCTION(BlueprintCallable)
		virtual void AddBomb(int BombsToAdd);

	UFUNCTION(BlueprintCallable)
		virtual void ChangeToPawnOverlap(int BombsToAdd);

	UFUNCTION(BlueprintCallable)
		virtual void ChangeToNormalPawn(int BombsToAdd);

	UFUNCTION(BlueprintCallable)
		virtual void TriggerBombs();

	void ReceiveExplosion();

	void IncreaseMovementSpeed(float Increase);
	void IncreaseBombCount(int Increase);
	void IncreaseTileRangeBomb(int Increase);
	void EnableRemoteBomb(bool Enable, float TimeEnabled);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerConfiguration)
		int PlayerNumber;

	UPROPERTY(EditDefaultsOnly, Category = PlayerConfiguration)
		float BaseMovementSpeed;

	UPROPERTY(VisibleAnywhere, Category = BombConfiguration)
		int BombsAvailable;

	UPROPERTY(EditDefaultsOnly, Category = BombConfiguration)
		int MaxBombs;

	UPROPERTY(EditDefaultsOnly, Category = BombConfiguration)
		int BaseTileRange;

	UPROPERTY(EditDefaultsOnly, Category = BombConfiguration)
		float BombTimeExplode;

	UPROPERTY(EditDefaultsOnly, Category = BombConfiguration)
		TSubclassOf<APlainBomb> BombSpawn;
private:

	float MovementSpeed;
	int TileRange;

	FTimerHandle RemoteBombsTimerHandle;
	bool RemoteBombsEnabled;

	FVector Horizontal;
	FVector Vertical;

	FVector GetNearestRoundedPositon(FVector position);

	TArray<AActor*> SpawnedBombs;
};
