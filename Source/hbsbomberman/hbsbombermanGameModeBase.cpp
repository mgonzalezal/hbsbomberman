// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "hbsbombermanGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Private/Characters/PlayerCharacter.h"
#include "Private/GameElements/PowerUps/PowerUp.h"

AhbsbombermanGameModeBase::AhbsbombermanGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AhbsbombermanGameModeBase::BeginPlay() 
{
	Super::BeginPlay();
	GameTimeLeft = GameTime;

	TArray<AActor*> Characters;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerCharacter::StaticClass(), Characters);

	for (size_t i = 0; i < Characters.Num(); i++)
	{
		APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(Characters[i]);
		SpawnedCharacters.FindOrAdd(PlayerCharacter->PlayerNumber, PlayerCharacter);
	}
}

void AhbsbombermanGameModeBase::Tick(float DeltaTime)
{
	if (GameTimeLeft <= 0.0f || SpawnedCharacters.Num() <= 1)
	{
		GameTimeLeft = 0.0f;
		int PlayerWinner = -1;
		if (SpawnedCharacters.Num() > 0) {
			TArray<int> Keys;
			SpawnedCharacters.GetKeys(Keys);
			PlayerWinner = Keys[0];
		}
		EndRound(SpawnedCharacters.Num() == 0 || (SpawnedCharacters.Num() > 1 && GameTimeLeft == 0.0f) ? EGameResult::GR_Draw : EGameResult::GR_Win, PlayerWinner);
		UGameplayStatics::SetGamePaused(GetWorld(), true);
	}
	else
	{
		GameTimeLeft -= DeltaTime;
	}

	Super::Tick(DeltaTime);
}

void AhbsbombermanGameModeBase::PlayerDead(int PlayerNumber, APlayerCharacter* PlayerCharacter)
{
	SpawnedCharacters.Remove(PlayerNumber);
}

void AhbsbombermanGameModeBase::SpawnRandomPickUP(FVector Location)
{
	if (rand() % 100 < SpawnChance)
	{
		FTransform transform;
		transform.SetLocation(Location);
		int RandomIndex = rand() % SpawnOptions.Num();
		if (APowerUp* PowerUp = GetWorld()->SpawnActor<APowerUp>(SpawnOptions[RandomIndex], transform))
		{
			PowerUp->SetImmune(true);
		}
	}
}