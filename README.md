# HBSBomberman

For this test I spend more or less 16 hours. 

All the content of this test is made by me apart from the meshes and textures that are taken from the original bomberman games.

Next things that could be added to the project if I decided to keep working on it would be:

-Generate random maps (Noise, patterns or just random)
-Work on AI (Generate movement nodes around on each tile, check for nearby bombs/powerups, etc..)
-Working more on controllers, right now the game is tied to 2 players because of the requirement of same keyboard, next thing would be to get a better system and allow more players with different controllers.
-More powerups (Punching bombs to move them around, slow other players, blasts can affect more than 1 wall, etc...)
-Implement negative power ups (Reduce movement, reduce bombs, etc...)
-Get neutral AI like monsters that walk around the map.
-Improve UI to show more information (Time left of the remote bomb power up for example)
-Implement particle effects for bomb blasts (Right now it is only debug of the line traces).
-Selection menu (Select diferent characters)

